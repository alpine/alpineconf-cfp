# AlpineConf CFP for 2021

Welcome to the GitLab repository for the AlpineConf Call for Proposals (CFP)
for 2021.

## Proposals closed

All talks were accepted for 2021.  At this point, you should pre-record your talk.
Please link to it in your bug once done.

BigBlueButton will be deployed later today, allowing for testing of the platform.

## When is AlpineConf 2021 occuring?

AlpineConf 2021 is presently planned to occur online on the weekend of May 15th
through May 16th.  This gives us two days worth of content so everyone should
be able to give a presentation who would like to.

## How do I submit proposals?

Open an issue with the following information:

 * The name of the presentation
 * How much time you would like for the presentation (25 or 50 minute slot)
 * A brief synopsis of what the presentation will cover

You should actively solicit feedback for your proposal!  While people who have
volunteered to help put this event on will eventually read the proposals, having
a more solid proposal will help us determine whether we should move forward with
it.

There will be two submission review rounds: April 23 and April 30.

## Can I submit multiple proposals?

You can submit proposals for multiple talks!  Talks that you want to do as an
alternate should have that noted in the description.

## My talk was accepted, what should I do?

At this point, you should pre-record your talk.  Once you're done with that,
you only need to wait until your talk is presented at AlpineConf.

Using BigBlueButton, we will play the pre-recorded talk and then users may
participate in an interactive Q&A session with you following the talk.

## My talk was rejected, what should I do?

If your talk was rejected in the first round, don't fret, you can ask for us
to reconsider it in the second round.  If it didn't make the second round,
you might be able to get a slot anyway if there's not enough content on the
second day.

## Code of Conduct

AlpineConf and its presenters are expected to abide by the [Alpine Code of Conduct][coc]
while participating in AlpineConf.

   [coc]: https://alpinelinux.org/community/code-of-conduct.html